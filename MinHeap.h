//
// Created by pinsk on 6/11/2018.
//

#ifndef MIVNEI2_MINHEAP_H
#define MIVNEI2_MINHEAP_H
#include "Clan.h"

class NOMIN{};
struct MinHeap{

    //number of elements in heap
    int n;

    //size of array
    int m;

    Clan** arr;

    int parent(int i){
        return i/2;
    }

    int left(int i){
        return 2*i;
    }

    int right(int i){
        return 2*i+1;
    }

   void swap(int i, int j){
       Clan* temp = arr[i];
       arr[i] = arr[j];
       arr[j] = temp;

       arr[i]->heap_location = i;
       arr[j]->heap_location = j;
    }

   void siftup(int i){

        int min;
        int l = left(i);
        int r = right(i);
        if(l <= n && arr[l]->id < arr[i]->id){
            min = l;
        }
        else{
            min = i;
        }
        if(r <= n && arr[r]->id < arr[min]->id){
            min = r;
        }
        if(min != i){
            swap(i, min);
            siftup(min);
        }

    }

    void buildHeap(){
        for(int i = n/2; i >=1; i--){
            siftup(i);
        }
    }

    int min(){
        if(n < 1){
            throw NOMIN();
        }
        return arr[1]->id;
    }

    void remove(int i){

        swap(i, n);
        n--;
        siftup(i);
    }

    void heapDouble(){
        m = 2*m;
        Clan** new_arr = new Clan*[m+1];

        for(int i = 1; i <= n; i++){
            new_arr[i] = arr[i];
        }

        delete[] arr;
        arr = new_arr;
    }

    void insert(Clan* x){

        if(n==m){
            heapDouble();
        }

        n++;
        arr[n] = x;
        x->heap_location = n;
        int i =n;
        while(i > 1 && arr[parent(i)]->id > arr[i]->id){
            swap(parent(i), i);
            i = parent(i);
        }
    }

    explicit MinHeap(int n) : n(n), m(2*n) {
        arr = new Clan*[m+1];
    }

    ~MinHeap(){
        delete[] arr;
    }





};
#endif //MIVNEI2_MINHEAP_H
