#include <iostream>
#include "TestMacros.h"
#include "../Oasis.h"

#define nullptr 0

using std::cout;

static Oasis* oasis;

struct players_t {
    PlayerID p1;
    PlayerID p2;
    PlayerID p3;
    PlayerID p4;
    PlayerID p5;
    PlayerID p6;
};

struct score_t {
    Score s1;
    Score s2;
    Score s3;
    Score s4;
    Score s5;
    Score s6;
};

struct clans_t {
    ClanID c1;
    ClanID c2;
    ClanID c3;
    ClanID c4;
    ClanID c5;
    ClanID c6;
};

static players_t players = {
        players.p1 = 1,
        players.p2 = 20,
        players.p3 = 300,
        players.p4 = 4000,
        players.p5 = 50000,
        players.p6 = 600000
};


static score_t scores = {
        scores.s1 = 1,
        scores.s2 = 20,
        scores.s3 = 300,
        scores.s4 = 4000,
        scores.s5 = 50000,
        scores.s6 = 600000
};


static clans_t clans = {
        clans.c1 = 1,
        clans.c2 = 20,
        clans.c3 = 300,
        clans.c4 = 4000,
        clans.c5 = 50000,
        clans.c6 = 600000
};


static void setUp() {
    oasis = new Oasis();
}

static void tearDown() {
    oasis->quit();
    delete oasis;
}

#define ZERO_CLAN 0

bool test_addClan() {

    setUp();

    // Invalid Input
    ASSERT_TRUE(oasis->addClan(-1) == INVALID_INPUT);

    // Success
    ASSERT_TRUE(oasis->addClan(clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(ZERO_CLAN) == SUCCESS);

    // Clan already in the system
    ASSERT_TRUE(oasis->addClan(clans.c1) == FAILURE);
    ASSERT_TRUE(oasis->addClan(ZERO_CLAN) == FAILURE);

    // insert all clans
    ASSERT_TRUE(oasis->addClan(clans.c2) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c3) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c4) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c5) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c6) == SUCCESS);

    // Already in the system
    ASSERT_TRUE(oasis->addClan(clans.c1) == FAILURE);
    ASSERT_TRUE(oasis->addClan(ZERO_CLAN) == FAILURE);
    ASSERT_TRUE(oasis->addClan(clans.c5) == FAILURE);

    tearDown();
    return true;
}

bool test_addPlayer() {
    setUp();

    // Invalid Input
    ASSERT_TRUE(oasis->addPlayer(-1, scores.s1, clans.c1) == INVALID_INPUT);
    ASSERT_TRUE(oasis->addPlayer(players.p1, -1, clans.c1) == INVALID_INPUT);
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, -1) == INVALID_INPUT);

    // No clans in the system
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, clans.c1) == FAILURE);
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, ZERO_CLAN) == FAILURE);

    // Clan in system, Add to not existed clan
    ASSERT_TRUE(oasis->addClan(ZERO_CLAN) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, clans.c1) == FAILURE);

    // Add to existed clan , add new clan
    ASSERT_TRUE(oasis->addClan(clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, ZERO_CLAN) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s1, clans.c1) == SUCCESS);

    // Player already in the system
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s1, clans.c1) == FAILURE);
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s1, ZERO_CLAN) == FAILURE);

    // Add all players except p6
    ASSERT_TRUE(oasis->addPlayer(players.p3, scores.s1, clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p4, scores.s1, ZERO_CLAN) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p5, scores.s1, clans.c1) == SUCCESS);

    // Player already in the system
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s1, clans.c1) == FAILURE);
    ASSERT_TRUE(oasis->addPlayer(players.p5, scores.s1, ZERO_CLAN) == FAILURE);

    // No Such Clan in the system
    ASSERT_TRUE(oasis->addPlayer(players.p6, scores.s1, clans.c3) == FAILURE);

    tearDown();
    return true;
}

bool test_clanFight() {
    setUp();

    // Add legal clans and players
    ASSERT_TRUE(oasis->addClan(ZERO_CLAN) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c2) == SUCCESS);

    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s2, clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p3, scores.s3, clans.c1) == SUCCESS);

    ASSERT_TRUE(oasis->addPlayer(players.p4, scores.s4, clans.c2) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p5, scores.s5, clans.c2) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p6, scores.s6, clans.c2) == SUCCESS);

    size_t clan_size = 3;

    // Illegal input
    ASSERT_TRUE(oasis->clanFight(-1, clans.c2, clan_size, clan_size) == INVALID_INPUT);
    ASSERT_TRUE(oasis->clanFight(clans.c1, -1, clan_size, clan_size) == INVALID_INPUT);

    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, -1, clan_size) == INVALID_INPUT);
    ASSERT_TRUE(oasis->clanFight(clans.c1, -1, clan_size, -1) == INVALID_INPUT);

    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, 0, clan_size) == INVALID_INPUT);
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size, 0) == INVALID_INPUT);

    // No such clan in the system
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c3, clan_size, clan_size) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c3, clans.c2, clan_size, clan_size) == FAILURE);

    // Less then k1,k2 players
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size + 1, clan_size) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size, clan_size + 1) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size + 1, clan_size + 1) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c1, ZERO_CLAN, clan_size + 1, clan_size) == FAILURE);

    // clan1 == clan2
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c1, clan_size, clan_size) == FAILURE);

    // Clean fight, c2 wins
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size, clan_size) == SUCCESS);

    // c1 can't compete anymore
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, clan_size, clan_size) == FAILURE);

    tearDown();
    return true;

}

bool test_getMinClan() {
    setUp();

    // Invalid Input
    ASSERT_TRUE(oasis->getMinClan(nullptr) == INVALID_INPUT);

    int min_clan;

    // No clans in the system
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == FAILURE);

    //Initial
    ASSERT_TRUE(oasis->addClan(clans.c1) == SUCCESS);

    // Single clan
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c1);

    // Add another clan and a single player per clan
    ASSERT_TRUE(oasis->addClan(clans.c2) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p1, scores.s1, clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addPlayer(players.p2, scores.s2, clans.c2) == SUCCESS);

    // c2 wins
    ASSERT_TRUE(oasis->clanFight(clans.c1, clans.c2, 1, 1) == SUCCESS);

    // After the fight c2 is the minimum
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c2);

    tearDown();
    return true;
}


bool test_case_1() {
    setUp();

    // Add legal clans and players
    ASSERT_TRUE(oasis->addClan(clans.c1) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c2) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c3) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c4) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c5) == SUCCESS);
    ASSERT_TRUE(oasis->addClan(clans.c6) == SUCCESS);

    int N = 10;
    int c1_size = N;
    int c2_size = N * 10;
    int c3_size = N * 15;
    int c4_size = N * 20;
    int c5_size = N * 25;
    int c6_size = N * 30;

    for (int i = 0; i < c1_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p1 + i, scores.s1 + i, clans.c1) == SUCCESS);
    }

    for (int i = 0; i < c2_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p2 + i, scores.s2 + i, clans.c2) == SUCCESS);
    }
    for (int i = 0; i < c3_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p3 + i, scores.s3 + i, clans.c3) == SUCCESS);
    }
    for (int i = 0; i < c4_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p4 + i, scores.s4 + i, clans.c4) == SUCCESS);
    }
    for (int i = 0; i < c5_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p5 + i, scores.s5 + i, clans.c5) == SUCCESS);
    }
    for (int i = 0; i < c6_size; ++i) {
        ASSERT_TRUE(oasis->addPlayer(players.p6 + i, scores.s6 + i, clans.c6) == SUCCESS);
    }



    int min_clan;

    // No fights, Min is clans.c1
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c1);

    //First Fight, c2 wins
    ASSERT_TRUE(oasis->clanFight(clans.c1,clans.c2, c1_size/2, c2_size) == SUCCESS);

    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c2);

    //c1 can't fight anymore
    ASSERT_TRUE(oasis->clanFight(clans.c1,clans.c5, c1_size/2, c5_size/2) == FAILURE);


    //c4 loses
    ASSERT_TRUE(oasis->clanFight(clans.c6,clans.c4, c6_size-30, c4_size/2) == SUCCESS);

    //still c2 is the minimum
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c2);

    //c2 loses, assign new minimum -> c3
    ASSERT_TRUE(oasis->clanFight(clans.c5,clans.c2, c5_size, c2_size/2) == SUCCESS);

    //still c2 is the minimum
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c3);

    //the rest lose to c6
    //c1 out
    ASSERT_TRUE(oasis->clanFight(clans.c1,clans.c6, c1_size, c6_size/2) == FAILURE);

    //c2 out
    ASSERT_TRUE(oasis->clanFight(clans.c2,clans.c6, c2_size, c6_size/2) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c3,clans.c6, c3_size, c6_size/2) == SUCCESS);

    //c4 out
    ASSERT_TRUE(oasis->clanFight(clans.c4,clans.c6, c4_size, c6_size/2) == FAILURE);
    ASSERT_TRUE(oasis->clanFight(clans.c5,clans.c6, c5_size, c6_size/2) == SUCCESS);

    //same clan
    ASSERT_TRUE(oasis->clanFight(clans.c6,clans.c6, c6_size, c6_size/2) == FAILURE);

    //only c6 left
    ASSERT_TRUE(oasis->getMinClan(&min_clan) == SUCCESS);
    ASSERT_TRUE(min_clan == clans.c6);

    tearDown();
    return true;
}

/*
bool test_template() {
    setUp();

    tearDown();
    return true;
}
*/

int main() {

    RUN_TEST(test_addClan);
    RUN_TEST(test_addPlayer);
    RUN_TEST(test_clanFight);
    RUN_TEST(test_getMinClan);

    RUN_TEST(test_case_1);

    return 0;
}