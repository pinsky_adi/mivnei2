//
// Created by pinsk on 6/11/2018.
//

#ifndef MIVNEI2_CLAN_H
#define MIVNEI2_CLAN_H

#include "AVL1.h"

struct Clan{

    class PLAYERALREADYEXIST{};
    class NOTENOUGHPLAYERS{};


    int id;
    int size;
    int heap_location;
    bool can_fight;

    AvlTree<int> byId;
    AvlTree<int> byScore;

    explicit Clan(int id) : id(id), size(0), heap_location(0), can_fight(true){};

    bool playerExists(int id){
        Node<int>* x = byId.search(id);

        if(NULL == x){
            return false;
        }

        return true;
    }

    void addPlayer(int id, int score){

        if(playerExists(id)){
            throw PLAYERALREADYEXIST();
        }

        byId.insert(id,NULL,score);
        byScore.insert(score,NULL,score);
        size++;
    }

    int getBestK(int k){
        if(k > size){
            throw NOTENOUGHPLAYERS();
        }
        return byScore.scoreOfBestK(k);
    }

};
#endif //MIVNEI2_CLAN_H
