//
// Created by pinsk on 6/12/2018.
//

#ifndef MIVNEI2_OASIS_H
#define MIVNEI2_OASIS_H

#include "MinHeap.h"
#include "Hash.h"
#include "Clan.h"

class INVALIDINPUT{};
class CLANNOTEXIST{};
class PLAYERALREDYEXIST{};
class CLAN_CANT_FIGHT{};
class NOT_ENOUGHT_PLAYERS{};
class SAME_CLAN{};

struct Oasis{

    int num_clans;
    int num_players;

    Hash hash;
    MinHeap heap;
    AvlTree<int> players;

    Oasis(int n, int* clans) : num_clans(n), num_players(0), hash(2*n), heap(n) {

        for(int i=0; i<n; i++){

            Clan* clan;

            try{
                clan = new Clan(clans[i]);
            }
            catch(std::bad_alloc&){
                for(int j=0;j<i; j++){
                    delete heap.arr[j];
                }
                num_clans=0;
            }

            heap.arr[i+1] = clan;
            clan->heap_location = i+1;
            hash.insert(clan);
        }
        heap.buildHeap();

    };

    void addClan(int id){
        if(id < 0){
            throw INVALIDINPUT();
        }

        Clan* clan = hash.search(id);

        if(NULL != clan){
            throw CLANALREADYEXIST();
        }



        clan = new Clan(id);

        hash.insert(clan);
        heap.insert(clan);
        num_clans++;

    }

    void addPlayer(int player_id, int score, int clan_id){

        if(clan_id < 0){
            throw INVALIDINPUT();
        }
        if(player_id < 0){
            throw INVALIDINPUT();
        }
        if(score < 0){
            throw INVALIDINPUT();
        }

        Node<int>* player = players.search(player_id);
        if(NULL != player){
            throw PLAYERALREDYEXIST();
        }

        Clan* clan = hash.search(clan_id);
        if(NULL == clan){
            throw CLANNOTEXIST();
        }

        players.insert(player_id,NULL);
        clan->addPlayer(player_id,score);
        num_players++;
    }

    void clanFight( int clan1_id, int clan2_id, int k1, int k2){
        if(k1<=0 || k2<=0 || clan1_id<0 ||clan2_id<0)
            throw INVALIDINPUT();

        Clan* clan1 = hash.search(clan1_id);
        Clan* clan2 = hash.search(clan2_id);
        if(NULL == clan1 || NULL == clan2){
            throw CLANNOTEXIST();
        }

        if(false == clan1->can_fight || false == clan2->can_fight )
            throw CLAN_CANT_FIGHT();

        if(clan1->size < k1 || clan2->size <k2)
            throw NOT_ENOUGHT_PLAYERS();

        if(clan1_id == clan2_id)
            throw SAME_CLAN();

        int sum1 = clan1->getBestK(k1);
        int sum2 = clan2->getBestK(k2);

        if(sum1 == sum2){
            if(clan1_id < clan2_id){
                clan2->can_fight = false;
                heap.remove(clan2->heap_location);
            }
            else{
                clan1->can_fight = false;
                heap.remove((clan1->heap_location));
            }
        }
        else if(sum1 < sum2){
            clan1->can_fight = false;
            heap.remove(clan1->heap_location);
        }
        else if(sum2 < sum1){
            clan2->can_fight = false;
            heap.remove(clan2->heap_location);
        }
    }

    int getMin(){
        return heap.min();
    }

    ~Oasis(){
        int i=0;
        int j=0;
        while(i < num_clans){
            if(!hash.arr[j].isEmpty()){
                listNode* x = hash.arr[j].head;
                while(NULL !=x){
                    delete x->clan;
                    x=x->next;
                    i++;
                }
            }
            j++;
        }
    }
};
#endif //MIVNEI2_OASIS_H
