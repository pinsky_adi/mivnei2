//
// Created by pinsk on 6/12/2018.
//

#include "library2.h"
#include "Oasis.h"

void* init(int n, int *clanIDs){


    if(NULL == clanIDs){
        return NULL;
    }

    if(2 > n){
        return NULL;
    }


    for(int i=0 ; i<n; i++){
        if(clanIDs[i] < 0)
            return NULL;

    }




    Oasis* oasis = new Oasis(n, clanIDs);

    return oasis;
}

StatusType addClan(void *DS, int clanID){
    if(NULL == DS){
        return INVALID_INPUT;
    }
    try{
        ((Oasis*)DS)->addClan(clanID);
    }
    catch(std::bad_alloc&){
        return ALLOCATION_ERROR;
    }
    catch(INVALIDINPUT&) {
        return INVALID_INPUT;
    }
    catch(CLANALREADYEXIST&){
        return FAILURE;
    }

    return SUCCESS;
}

StatusType addPlayer(void *DS, int playerID, int score, int clan){
    if(NULL == DS){
        return INVALID_INPUT;
    }
    try{
        ((Oasis*)DS)->addPlayer(playerID,score,clan);
    }
    catch(std::bad_alloc&){
        return ALLOCATION_ERROR;
    }
    catch(INVALIDINPUT&){
        return INVALID_INPUT;
    }
    catch(PLAYERALREDYEXIST&){
        return FAILURE;
    }
    catch(CLANNOTEXIST&){
        return FAILURE;
    }

    return SUCCESS;
}

StatusType clanFight(void *DS, int clan1, int clan2, int k1, int k2){

    if(NULL == DS){
        return INVALID_INPUT;
    }
    try{
        ((Oasis*)DS)->clanFight(clan1,clan2,k1,k2);
    }
    catch(std::bad_alloc&){
        return ALLOCATION_ERROR;
    }
    catch(INVALIDINPUT&){
        return INVALID_INPUT;
    }
    catch(CLAN_CANT_FIGHT&){
        return FAILURE;
    }
    catch(CLANNOTEXIST&){
        return FAILURE;
    }
    catch(SAME_CLAN&){
        return FAILURE;
    }
    catch(NOT_ENOUGHT_PLAYERS&){
        return FAILURE;
    }

    return SUCCESS;
}

StatusType getMinClan(void *DS, int *clan){
    if(NULL == DS || NULL == clan){
        return INVALID_INPUT;
    }
    *clan = ((Oasis*)DS)->getMin();

    return SUCCESS;
}

void quit(void** DS){
    if(NULL == DS || *DS == NULL)
        return;
    delete ((Oasis*)(*DS));
   *DS = NULL;
}