//
//
// Created by pinsk on 5/8/2018.
//
#include<iostream>
#include "AVL1.h"
#include "Hash.h"
#include "Clan.h"
using std::cout;
using std::endl;

#include "testMacros.h"
#include "MinHeap.h"


class function{
public:
    bool operator()(Node<int> x){
        return true;
    }
};
bool testInsert(){
    AvlTree<int> tree;
    tree.insert(4,NULL,15);
    tree.insert(2,NULL,5);
    tree.insert(3,NULL,0);
    tree.insert(9,NULL,3);
    tree.insert(5,NULL,30);
    tree.insert(1,NULL,10);
    cout<<endl<<endl<<"---------------------tree 1--------------------"<<endl;
    cout<<tree;
    /*expected output
Key1 is: 1 Key2 is: 0 Height is: 0
Key1 is: 2 Key2 is: 0 Height is: 1
Key1 is: 3 Key2 is: 0 Height is: 2
Key1 is: 4 Key2 is: 0 Height is: 0
Key1 is: 5 Key2 is: 0 Height is: 1
Key1 is: 9 Key2 is: 0 Height is: 0
    **/

    AvlTree<int> tree2;
    cout<<endl<<endl<<"---------------------tree 2--------------------"<<endl;
    cout<<tree2;

    /*expected output
   **/

    AvlTree<int> tree3;
    tree3.insert(1,NULL);
    tree3.insert(2,NULL);
    tree3.insert(3,NULL);
    cout<<endl<<endl<<"---------------------tree 3--------------------"<<endl;
    cout<<tree3;

    /*expected output
  Key1 is: 1 Key2 is: 0 Height is: 0
Key1 is: 2 Key2 is: 0 Height is: 1
Key1 is: 3 Key2 is: 0 Height is: 0
   **/

    AvlTree<int> tree4;
    tree4.insert(3,NULL);
    tree4.insert(2,NULL);
    tree4.insert(1,NULL);
    cout<<endl<<endl<<"---------------------tree 4--------------------"<<endl;
    cout<<tree4;

    /*expected output
Key1 is: 1 Key2 is: 0 Height is: 0
Key1 is: 2 Key2 is: 0 Height is: 1
Key1 is: 3 Key2 is: 0 Height is: 0

*   */

    AvlTree<int> tree5;
    tree5.insert(2,NULL);
    tree5.insert(3,NULL);
    tree5.insert(1,NULL);
    cout<<endl<<endl<<"---------------------tree 5--------------------"<<endl;
    cout<<tree5;

    /*Key1 is: 1 Key2 is: 0 Height is: 0
Key1 is: 2 Key2 is: 0 Height is: 1
Key1 is: 3 Key2 is: 0 Height is: 0
     */

    return true;
}

bool testRemove(){
    AvlTree<int> tree1;

    cout<<endl<<"-------------------testremove1----------------"<<endl<<endl;
    tree1.remove(0);
    tree1.insert(1,NULL);
    //should do nothing
    tree1.remove(2);
    cout<<endl<<"-------------------testremove2----------------"<<endl<<endl;
    cout<<tree1;
    /*Key1 is: 1 Key2 is: 0 Height is: 0*/

    cout<<endl<<"-------------------testremove3----------------"<<endl<<endl;
    tree1.remove(1);
    cout<<tree1<<endl;
    //nothing
    tree1.insert(1,NULL);
    tree1.insert(2,NULL);
    tree1.remove(1);
    cout<<endl<<"-------------------testremove4----------------"<<endl<<endl;
    cout<<tree1<<endl;
    /*Key1 is: 2 Key2 is: 0 Height is: 0*/

    AvlTree<int> tree2;
    tree2.insert(2,NULL);
    tree2.insert(1,NULL);
    tree2.insert(8,NULL);
    tree2.insert(6,NULL);
    tree2.insert(5,NULL);
    tree2.insert(10,NULL);
    tree2.insert(9,NULL);
    tree2.remove(2);
    cout<<endl<<"-------------------testremove5----------------"<<endl<<endl;
    cout<<tree2;
    /*Key1 is: 1 Key2 is: 0 Height is: 0
Key1 is: 5 Key2 is: 0 Height is: 1
Key1 is: 6 Key2 is: 0 Height is: 2
Key1 is: 8 Key2 is: 0 Height is: 0
Key1 is: 9 Key2 is: 0 Height is: 1
Key1 is: 10 Key2 is: 0 Height is: 0*/

    tree2.remove(1);
    cout<<endl<<"-------------------testremove6----------------"<<endl<<endl;
    cout<<tree2;

    /*   Key1 is: 5 Key2 is: 0 Height is: 0
   Key1 is: 6 Key2 is: 0 Height is: 2
   Key1 is: 8 Key2 is: 0 Height is: 0
   Key1 is: 9 Key2 is: 0 Height is: 1
   Key1 is: 10 Key2 is: 0 Height is: 0*/

    tree2.remove(5);
    cout<<endl<<"-------------------testremove7----------------"<<endl<<endl;
    cout<<tree2;

    /*  Key1 is: 6 Key2 is: 0 Height is: 0
Key1 is: 8 Key2 is: 0 Height is: 2
Key1 is: 9 Key2 is: 0 Height is: 1
Key1 is: 10 Key2 is: 0 Height is: 0*/


    tree2.remove(6);
    cout<<endl<<"-------------------testremove8----------------"<<endl<<endl;
    cout<<tree2;

/*Key1 is: 8 Key2 is: 0 Height is: 0
Key1 is: 9 Key2 is: 0 Height is: 1
Key1 is: 10 Key2 is: 0 Height is: 0
 */

    tree2.remove(9);
    cout<<endl<<"-------------------testremove9----------------"<<endl<<endl;
    cout<<tree2;
/*Key1 is: 8 Key2 is: 0 Height is: 0
Key1 is: 10 Key2 is: 0 Height is: 1*/


    tree2.remove(8);
    cout<<endl<<"-------------------testremove10----------------"<<endl<<endl;
    cout<<tree2;
/*Key1 is: 8 Key2 is: 0 Height is: 0*/


    return true;
}



bool tree2array(){
    AvlTree<int> tree;
    tree.insert(4,NULL);
    tree.insert(2,NULL);
    tree.insert(3,NULL);
    tree.insert(9,NULL);
    tree.insert(5,NULL);
    tree.insert(1,NULL);
    Node<int>* arr = new Node<int>[tree.getSize()];
    tree.tree2array(arr);
    for(int i=0;i<6;i++){
        cout<<(arr+i)->key1;
    }
    cout<<endl;
    delete(arr);
    return true;
}

bool array2tree(){
    AvlTree<int> tree;
    tree.insert(4,NULL);
    tree.insert(2,NULL);
    tree.insert(3,NULL);
    tree.insert(9,NULL);
    tree.insert(5,NULL);
    tree.insert(1,NULL);
    Node<int>* arr = new Node<int>[tree.getSize()];
    tree.tree2array(arr);
    AvlTree<int> new_tree;
    array2tree(new_tree,arr,tree.getSize());
    cout<< new_tree;
    delete(arr);
    return true;
}

bool mergeArrays(){
    AvlTree<int> tree;
    tree.insert(4,NULL);
    tree.insert(2,NULL);
    tree.insert(3,NULL);
    tree.insert(9,NULL);
    tree.insert(5,NULL);
    tree.insert(1,NULL);
    Node<int>* arr = new Node<int>[tree.getSize()];
    tree.tree2array(arr);

    AvlTree<int> tree1;
    tree1.insert(-10,NULL);
    tree1.insert(100,NULL);
    tree1.insert(102,NULL);

    Node<int>* arr1 = new Node<int>[tree1.getSize()];
    tree1.tree2array(arr1);

    Node<int>* arr2 = new Node<int>[tree.getSize() + tree1.getSize()];


    mergeArrays(arr,arr1,6,3,arr2);

    for(int i=0; i<6; i++){
        cout<<arr[i].key1;
        cout<<" ";
    }
    cout<<endl;

    for(int i=0; i<3; i++){
        cout<<arr1[i].key1;
        cout<<" ";
    }
    cout<<endl;
    for(int i=0; i<9; i++){
        cout<<arr2[i].key1;
        cout<<" ";
    }

    cout<<endl;
    delete[](arr);
    delete[](arr1);
    delete[](arr2);
    return true;
}

bool testMergeTree(){

    AvlTree<int> tree;
    tree.insert(4,NULL);
    tree.insert(2,NULL);
    tree.insert(3,NULL);
    tree.insert(9,NULL);
    tree.insert(5,NULL);
    tree.insert(1,NULL);

    AvlTree<int> tree2;
    tree2.insert(100,NULL);
    tree2.insert(101,NULL);
    tree2.insert(102,NULL);
    tree2.insert(103,NULL);
    tree2.insert(104,NULL);
    tree2.insert(105,NULL);

    AvlTree<int> tree3;

    mergeTrees(tree,tree2,tree3,function());
    cout<<tree3;


    return true;
}



bool testSelect(){
    AvlTree<int> tree;
    tree.insert(4,NULL,15);
    tree.insert(2,NULL,5);
    tree.insert(3,NULL,0);
    tree.insert(9,NULL,3);
    tree.insert(5,NULL,30);
    tree.insert(1,NULL,10);

    Node<int>* a=tree.Select(1);
    ASSERT_TRUE(a->key1==9);

    a=tree.Select(2);
    ASSERT_TRUE(a->key1=5);

    a=tree.Select(3);
    ASSERT_TRUE(a->key1=4);

    a=tree.Select(4);
    ASSERT_TRUE(a->key1=3);

    a=tree.Select(5);
    ASSERT_TRUE(a->key1=2);

    a=tree.Select(6);
    ASSERT_TRUE(a->key1=1);

    a=tree.Select(-2);
    ASSERT_TRUE(a==NULL);

    a=tree.Select(0);
    ASSERT_TRUE(a==NULL);

    a=tree.Select(50);
    ASSERT_TRUE(a==NULL);


    return  true;
}


bool testscoreOfBestK(){

    AvlTree<int> tree;
    tree.insert(4,NULL,15);
    tree.insert(2,NULL,5);
    tree.insert(3,NULL,0);
    tree.insert(9,NULL,3);
    tree.insert(5,NULL,30);
    tree.insert(1,NULL,10);

    ASSERT_TRUE(tree.scoreOfBestK(0) == -1);
    ASSERT_TRUE(tree.scoreOfBestK(-1) == -1);

    ASSERT_TRUE(tree.scoreOfBestK(1) == 3);
    ASSERT_TRUE(tree.scoreOfBestK(2) == 33);
    ASSERT_TRUE(tree.scoreOfBestK(3) == 48);
    ASSERT_TRUE(tree.scoreOfBestK(4) == 48);
    ASSERT_TRUE(tree.scoreOfBestK(5) == 53);
    ASSERT_TRUE(tree.scoreOfBestK(6) == 63);
    return true;
}

bool testAddPlayer(){
    Clan c(1);
    c.addPlayer(4,15);
    c.addPlayer(2,5);
    c.addPlayer(3,0);
    c.addPlayer(9,3);
    c.addPlayer(5,30);
    c.addPlayer(1,10);


    ASSERT_TRUE(c.getBestK(1) == 30);
    ASSERT_TRUE(c.getBestK(2) == 45);
    ASSERT_TRUE(c.getBestK(3) == 55);
    ASSERT_TRUE(c.getBestK(4) == 60);
    ASSERT_TRUE(c.getBestK(5) == 63);
    ASSERT_TRUE(c.getBestK(6) == 63);


    return true;
}

bool testHash(){
    Clan c(1);
    c.addPlayer(4,15);
    c.addPlayer(2,5);
    c.addPlayer(3,0);
    c.addPlayer(9,3);
    c.addPlayer(5,30);
    c.addPlayer(1,10);




    Hash a(5);
    a.insert(&c);


    Clan c3(2);
    c3.addPlayer(9,3);
    c3.addPlayer(5,30);
    c3.addPlayer(1,10);
    a.insert(&c3);

    Clan c4(3);
    c4.addPlayer(9,3);
    c4.addPlayer(5,30);
    c4.addPlayer(1,10);
    a.insert(&c4);


    Clan c5(4);
    c5.addPlayer(9,3);
    c5.addPlayer(5,30);
    c5.addPlayer(1,10);

    a.insert(&c5);

    Clan c6(5);
    c6.addPlayer(9,3);
    c6.addPlayer(5,30);
    c6.addPlayer(1,10);

    a.insert(&c6);

    Clan c7(6);
    c7.addPlayer(9,3);
    c7.addPlayer(5,30);
    c7.addPlayer(1,10);

    a.insert(&c7);

    ASSERT_TRUE(a.m == 10);
    ASSERT_TRUE(a.n == 6);

    ASSERT_TRUE(&c7==a.search(6))
    ASSERT_TRUE(&c5==a.search(4))


    ASSERT_TRUE(a.search(50) == NULL)

    ASSERT_EXCEPTION(a.insert(&c),CLANALREADYEXIST);



    return true;
}

bool testMinHeap(){
    MinHeap a(5);
    Clan c1(1);
    Clan c2(2);
    Clan c3(3);
    Clan c4(4);
    Clan c5(5);

    a.arr[2]=&c2;
    a.arr[3]=&c3;
    a.arr[4]=&c4;
    a.arr[5]=&c5;
    a.arr[1]=&c1;
    a.buildHeap();
    ASSERT_TRUE(a.min()==1);
    a.remove(1);
    ASSERT_TRUE(a.min()==2);
    ASSERT_TRUE(a.n == 4);
    ASSERT_TRUE(a.m == 10);
    a.insert(&c1);
    ASSERT_TRUE(a.n == 5);
    ASSERT_TRUE(a.min()==1);

    Clan c8(8);
    Clan c6(6);
    Clan c7(7);
    Clan c9(9);
    Clan c10(10);

    a.insert(&c8);
    a.insert(&c6);
    a.insert(&c7);
    a.insert(&c9);
    a.insert(&c10);
    ASSERT_TRUE(a.min() == 1);
    ASSERT_TRUE(a.n == 10);
    ASSERT_TRUE(a.m == 10);


    Clan c11(0);
    a.insert(&c11);
    ASSERT_TRUE(a.min() == 0);
    ASSERT_TRUE(a.n == 11);
    ASSERT_TRUE(a.m == 20);

    a.remove(1);
    a.remove(1);
    a.remove(1);
    a.remove(1);
    ASSERT_TRUE(a.min()==4);



    return true;
}

int main() {
  /*  RUN_TEST(testInsert);
    RUN_TEST(testRemove);

    RUN_TEST(tree2array);
    RUN_TEST(array2tree);
    RUN_TEST(mergeArrays);
    RUN_TEST(testMergeTree);


    RUN_TEST(testSelect);
    RUN_TEST(testscoreOfBestK)
    RUN_TEST(testAddPlayer);
    RUN_TEST(testHash)
    */
    RUN_TEST(testMinHeap);

}
