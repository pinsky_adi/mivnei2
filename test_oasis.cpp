//
// Created by pinsk on 6/12/2018.
//

#include "testMacros.h"

#include "library2.h"

bool testInit(){
    int clanids[5];
    for(int i=0;i<5;i++){
        clanids[i]=i;
    }
    void* sys=init(5,clanids);



    ASSERT_TRUE(init(2,NULL)==NULL);
    ASSERT_TRUE(init(1,clanids)==NULL);
    quit(&sys);
    return true;
}

bool testaddClan(){
    int clanids[5];
    for(int i=0;i<5;i++){
        clanids[i]=i;
    }
    void* sys=init(5,clanids);
    ASSERT_TRUE(addClan(sys,0)==FAILURE);
    ASSERT_TRUE(addClan(sys,1) == FAILURE);
    ASSERT_TRUE(addClan(sys,-1) == INVALID_INPUT);
    ASSERT_TRUE(addClan(NULL,8) == INVALID_INPUT);

    ASSERT_TRUE(addClan(sys,6) == SUCCESS);
    ASSERT_TRUE(addClan(sys,7) == SUCCESS);

    quit(&sys);
    return true;
}


bool testaddPlayer(){

    int clanids[5];
    for(int i=0;i<5;i++){
        clanids[i]=i;
    }
    void* sys=init(5,clanids);

    ASSERT_TRUE(addPlayer(NULL, 1, 1, 1) == INVALID_INPUT);
    ASSERT_TRUE(addPlayer(sys, -1, 1, 1) == INVALID_INPUT);
    ASSERT_TRUE(addPlayer(sys, 1, -1, 1) == INVALID_INPUT);
    ASSERT_TRUE(addPlayer(sys, 1, 1, -1) == INVALID_INPUT);

    ASSERT_TRUE(addPlayer(sys, 1, 1, 6) == FAILURE);

    ASSERT_TRUE(addPlayer(sys, 1, 1, 1) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 1, 1, 1) ==FAILURE);

    ASSERT_TRUE(addPlayer(sys, 1, 1, 2) == FAILURE);

    ASSERT_TRUE(addPlayer(sys, 2, 1, 2) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 2, 1, 2) == FAILURE);
    ASSERT_TRUE(addPlayer(sys, 2, 1, 3) == FAILURE);

    quit(&sys);
  return true;
}



bool testclanFight(){
    int clanids[6];
    for(int i=0;i<7;i++){
        clanids[i]=i;
    }

    void* sys=init(7,clanids);
    ASSERT_TRUE(addPlayer(sys, 1, 10, 1) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 2, 20, 2) == SUCCESS);

    ASSERT_TRUE(clanFight(NULL, 1, 2, 1, 1) == INVALID_INPUT);
    ASSERT_TRUE(clanFight(sys, 1, 2, -1, 1) == INVALID_INPUT);
    ASSERT_TRUE(clanFight(sys, 1, 2, 1, -1) == INVALID_INPUT);
    ASSERT_TRUE(clanFight(sys, 1, 2, 1,0) == INVALID_INPUT);
    ASSERT_TRUE(clanFight(sys, 1, 2, 0,1) == INVALID_INPUT);

    ASSERT_TRUE(clanFight(sys, 1,7, 1,1) == FAILURE);
    ASSERT_TRUE(clanFight(sys, 7,2, 1,1) == FAILURE);

    StatusType clanFight(void *DS, int clan1, int clan2, int k1, int k2);

    ASSERT_TRUE(clanFight(sys, 1,2, 2,1) == FAILURE);
    ASSERT_TRUE(clanFight(sys, 1,2, 1,2) == FAILURE);

    ASSERT_TRUE(clanFight(sys, 1,1, 1,1) == FAILURE);

    ASSERT_TRUE(addPlayer(sys, 3, 10, 3) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 4, 20, 3) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 5, 30, 3) == SUCCESS);


    ASSERT_TRUE(addPlayer(sys, 6, 10, 4) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 7, 20, 4) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 8, 30, 4) == SUCCESS);

    ASSERT_TRUE(clanFight(sys, 3,4, 3,3) == SUCCESS);
    ASSERT_TRUE(clanFight(sys, 3,4, 3,3) == FAILURE);

    ASSERT_TRUE(addPlayer(sys, 9, 10,6) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 10, 20, 6) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 11, 30, 6) == SUCCESS);


    ASSERT_TRUE(addPlayer(sys, 12, 10, 5) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 13, 20, 5) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 14, 29, 5) == SUCCESS);

    ASSERT_TRUE(clanFight(sys, 5,6, 3,3) == SUCCESS);
    ASSERT_TRUE(clanFight(sys,5,4,3,3)==FAILURE);

    quit(&sys);
    return true;
}

bool testGetMin(){
    int clanids[6];
    for(int i=0;i<7;i++){
        clanids[i]=i;
    }
    void* sys=init(7,clanids);
    int clan =-1;

    getMinClan(sys,&clan);
    ASSERT_TRUE(clan == 0);

    ASSERT_TRUE(addPlayer(sys, 12, 10, 0) == SUCCESS);
    ASSERT_TRUE(addPlayer(sys, 13, 20, 1) == SUCCESS);

    ASSERT_TRUE(clanFight(sys, 0,1,1,1) == SUCCESS);
    getMinClan(sys,&clan);
    ASSERT_TRUE(clan == 1);
    quit(&sys);
    return true;

}

int main() {

    RUN_TEST(testInit);
 RUN_TEST(testaddClan);
   RUN_TEST(testaddPlayer);
    RUN_TEST(testclanFight);
    RUN_TEST(testGetMin);

}