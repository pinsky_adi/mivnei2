//
// Created by pinsk on 5/8/2018.
//

#ifndef MIVNEI_AVL1_H
#define MIVNEI_AVL1_H

#include <iostream>
#include <assert.h>
using std::ostream;
using std::endl;
using std::cout;

inline int max(int a, int b){
    if(a>b) return a;
    return b;
}

template<typename Type>
struct Node{

    Type* satellite;

    int key1;
    int key2;

    int height;
    int rank;
    int score_rank;

    int score;

    Node* right_child;
    Node* left_child;
    Node* parent;

    //normal usage constructor
    explicit Node(int key1, Type* satellite, int score=0, int key2=0)
            : satellite(satellite), key1(key1), key2(key2), height(0),rank(0),
              score_rank(0), score(score), right_child(NULL), left_child(NULL), parent(NULL) {};

    //constructor used for creating array
    explicit Node() : satellite(NULL), key1(0), key2(0), height(0), right_child(NULL),
                      left_child(NULL), parent(NULL){};


    Node& operator=(const Node& rhs){
        key1 = rhs.key1;
        key2 = rhs.key2;
        satellite = rhs.satellite;
        return *this;
    }

    friend bool operator<(const Node& lhs, const Node& rhs){
        if(lhs.key1 < rhs.key1) return true;
        if(lhs.key1 > rhs.key1) return false;

        //we are here if and only if lhs.key1 == rhs.key1

        if(lhs.key2 > rhs.key2)
            return true;
        return false;
    }

    friend bool operator>(const Node& lhs, const Node& rhs){
        if(lhs.key1 > rhs.key1) return true;
        if(lhs.key1 < rhs.key1) return false;

        //we are here if and only if lhs.key1 == rhs.key1

        if(lhs.key2 < rhs.key2) return true;
        return false;
    }

    friend bool operator==(const Node& lhs, const Node& rhs ){
        return !(lhs < rhs) && !(rhs < lhs);
    }
};

template<typename Type>
struct AvlTree{
private:






    void tree2arrayAux( Node<Type>* &arr, Node<Type>* x){

        assert(NULL != arr);
        if(NULL != x){
            tree2arrayAux(arr,x->left_child);
            *arr = *x;
            arr ++;
            tree2arrayAux(arr,x->right_child);
        }
    }

    //constructor
public:
    Node<Type>* root;
    int size;

    void postOrderDelete(Node<Type>* x){
        if(NULL != x) {
            postOrderDelete(x->left_child);
            postOrderDelete(x->right_child);
            delete (x);
            x=NULL;
        }
    }

    AvlTree() : root(NULL), size(0){};

    int getSize(){
        return size;
    }

    //return rank of node
    int rank(Node<Type>* x){
        if(NULL == x)
            return 0;
        return x->rank;
    }

    void updateRank(Node<Type>* x){
        x->rank = (rank(x->left_child) + rank(x->right_child) + 1);
    }

    int scoreRank(Node<Type>* x){
        if(NULL == x)
            return 0;
        return x->score_rank;
    }

    void updateScoreRank(Node<Type>* x){
        x->score_rank = (scoreRank(x->left_child) + scoreRank(x->right_child) + x->score);
    }

    //return height of node
    int height(Node<Type>* x){
        if(NULL == x)
            return -1;
        return x->height;
    }

    void updateHeight(Node<Type>* x){
        x->height= (max(height(x->left_child), height(x->right_child)) +1);
    }

    void recursiveUpdateHeight(Node<Type>* x){
        if(NULL != x){
            recursiveUpdateHeight(x->left_child);
            recursiveUpdateHeight(x->right_child);
            updateHeight(x);
        }
    }

    //clears satellite data in tree rooted at listNode x
    void clearData(Node<Type> *x){
        if(NULL != x){
            clearData(x->left_child);
            if(NULL != x->satellite){
                delete(x->satellite);
            }
            clearData(x->right_child);
        }
    }

    //clears satellite data in entire tree
    void clearData(){
        clearData(root);
    }

    //search in subtree rooted at listNode x
    Node<Type>* search(Node<Type> *x, int key1, int key2 = 0){

        Node<Type> y = Node<Type>(key1,NULL,key2);
        if(NULL == x || *x == y )
            return  x;

        if(y < *x)
            return search(x->left_child, key1, key2);
        return search(x->right_child, key1, key2);
    }

    //search entire tree
    Node<Type>* search(int key1, int key2=0){
        return search(root, key1, key2);
    }


    //finds minimum element in subtree rooted at x
    Node<Type>* minimum(Node<Type>* x){

        if(NULL == x) return NULL;
        while(NULL != x->left_child){
            x=x->left_child;
        }
        return x;
    }

    //finds immediate successor node x
    Node<Type>* successor(Node<Type>* x){
        if(NULL == x) return NULL;

        if(NULL != x->right_child) return minimum(x->right_child);
        Node<Type>* y = x->parent;
        while(NULL != y && x==y->right_child){
            x=y;
            y=y->parent;
        }

        return y;
    }

    void rotateLeft(Node<Type>* x) {
        assert(NULL != x && NULL != x->right_child);

        Node<Type>* y= x->right_child;
        x->right_child = y->left_child;
        if(y->left_child!=NULL)
            (y->left_child)->parent = x;
        y->parent = x->parent;

        if(x->parent == NULL) root = y;
        else{
            if(x == (x->parent)->left_child){
                (x->parent)->left_child = y;
            }
            else{
                (x->parent)->right_child = y;
            }
        }

        y->left_child = x;
        x->parent = y;

        updateHeight(x);
        updateHeight(y);

        updateRank(x);
        updateScoreRank(x);

        updateRank(y);
        updateScoreRank(y);

    }

    void rotateRight(Node<Type>* x) {
        assert(NULL != x && NULL != x->left_child);

        Node<Type>* y= x->left_child;
        x->left_child = y->right_child;
        if(y->right_child!=NULL)
            (y->right_child)->parent = x;
        y->parent = x->parent;

        if(x->parent == NULL) root = y;
        else{
            if(x == (x->parent)->left_child){
                (x->parent)->left_child = y;
            }
            else{
                (x->parent)->right_child = y;
            }
        }

        y->right_child = x;
        x->parent = y;

        updateHeight(x);
        updateHeight(y);

        updateRank(x);
        updateScoreRank(x);

        updateRank(y);
        updateScoreRank(y);

    }

    void rebalance(Node<Type>* x){

        while(NULL != x){
            updateHeight(x);
            updateRank(x);
            updateScoreRank(x);

            if(height(x->left_child) >= 2 + height(x->right_child)){
                if(height((x->left_child)->left_child) >= height((x->left_child)->right_child)){
                    rotateRight(x);
                }
                else{
                    rotateLeft(x->left_child);
                    rotateRight(x);
                }
            }
            else{
                if(height(x->right_child) >= 2 + height(x->left_child)){
                    if(height((x->right_child)->right_child) > height((x->right_child)->left_child)){
                        rotateLeft(x);
                    }
                    else{
                        rotateRight(x->right_child);
                        rotateLeft(x);
                    }
                }
            }

            x=x->parent;
        }
    }

    void insert(int key1, Type* satellite, int score=0, int key2=0){

        //create new node
        Node<Type>* z = new Node<Type>(key1,satellite, score, key2);

        Node<Type>* y = NULL;
        Node<Type>* x = root;
        while(NULL != x){
            y=x;
            if(*z < *x)
                x=x->left_child;
            else
                x=x->right_child;
        }
        z->parent=y;
        //if tree is empty
        if(NULL == y)
            root = z;
        else{
            if(*z < *y)
                y->left_child = z;
            else
                y->right_child =z;
        }

        rebalance(z);
        size++;
    }

    void remove(int key1, int key2=0){

        Node<Type>* z = search(root, key1, key2);
        if(NULL == z) return;

        //the actual node to be deleted
        Node<Type>* y = NULL;

        //the node to be linked to y's parent
        Node<Type>* x = NULL;

        if(NULL == z->left_child || NULL == z->right_child)
            y=z;
        else
            y= successor(z);

        if(NULL != y->left_child)
            x = y->left_child;
        else
            x=y->right_child;
        if(NULL != x) x->
                    parent = y->parent;
        if(NULL == y->parent)
            root = x;
        else{
            if(y==(y->parent)->left_child){
                (y->parent)->left_child = x;
            }
            else{
                (y->parent)->right_child = x;
            }
        }
        if(y != z){
            z->key1 = y->key1;
            z->key2 = y->key2;
            z->satellite = y->satellite;
        }

        if(NULL != y->parent)
            rebalance(y->parent);
        delete(y);
        size--;
    }

    void inOrderPrint(ostream& os, Node<Type>* x ) const {
        if(x!=NULL){
            inOrderPrint(os, x->left_child);
            os<<"Key1 is: " <<x->key1<< " Key2 is: "<< x->key2<< " Height is: "<< x->height<<"       ";

            os<<"rank is: " <<x->rank<< " score rank is: "<< x->score_rank<<endl;
            inOrderPrint(os, x->right_child);
        }
    }
   friend ostream& operator<<(ostream& os, const AvlTree& a){
       a.inOrderPrint(os, a.root);
       return os;
   }

    //This function allocates memory which will need to be deleted!
    void tree2array(Node<Type>* &arr){
        if(0 == this->getSize()){
            return;
        }

        Node<Type>* temp = arr;
        tree2arrayAux(temp,root);
    }


    //
    Node<Type>* Select(int i){
        if(i > size || i <= 0){
            return NULL;
        }
        return SelectAux(root, size - i+1);
    }

    Node<Type>* SelectAux(Node<Type>* x, int i){
        if(x == NULL){
            return NULL;
        }
        int r = rank(x->left_child) + 1;
        if(i ==r){
            return x;
        }
        else if(i < r){
           return SelectAux(x->left_child, i);
        }
        else {
            return SelectAux(x->right_child,i-r);
        }

    }

    int scoreOfBestK(int k){
        Node<Type>* x = Select(k);

        if(NULL == x){
            return -1;
        }

        int r = scoreRank(x->right_child) + x->score;
        Node<Type>* y = x;
        while(y != root){
            if(y == y->parent->left_child){
                r =r+ scoreRank(y->parent->right_child) + y->parent->score;
            }
            y = y->parent;
        }

        return r;
    }



    ~AvlTree(){
        postOrderDelete(root);
    }


};

template<typename  Type>
Node<Type>* array2treeAux(Node<Type>* arr, int start, int end){

    if(start > end){
        return NULL;
    }

    int mid = (start + end)/2;
    Node<Type>* node = new Node<Type>;
    *node = *(arr + mid);
    node->left_child = array2treeAux(arr,start,mid-1);
    node->right_child = array2treeAux(arr,mid+1,end);

    if(NULL != node->left_child){
        (node->left_child)->parent = node;
    }
    if(NULL != node->right_child){
        (node->right_child)->parent = node;
    }

    return node;
}

template<typename Type>
void array2tree(AvlTree<Type>& tree, Node<Type>* arr, int size){

    tree.root = array2treeAux(arr,0,size-1);
    tree.recursiveUpdateHeight(tree.root);
    tree.size = size;
}

template<typename Type>
void mergeArrays(Node<Type> arr1[], Node<Type> arr2[], int size1,
                 int size2, Node<Type> arr3[])
{
    assert(NULL != arr1 && NULL!= arr2 && NULL!= arr3);
    int i = 0, j = 0, k = 0;

    while (i<size1 && j <size2)
    {
        if (arr1[i] < arr2[j])
            arr3[k++] = arr1[i++];
        else
            arr3[k++] = arr2[j++];
    }

    while (i < size1)
        arr3[k++] = arr1[i++];
    while (j < size2)
        arr3[k++] = arr2[j++];
}

template<typename Type, typename  Function>
void mergeTrees( AvlTree<Type>& tree1,  AvlTree<Type>& tree2, AvlTree<Type>& merge_tree,
Function func){

    Node<Type>* arr1 = new Node<Type>[tree1.getSize()];
    Node<Type>* arr2 = new Node<Type>[tree2.getSize()];
    Node<Type>* arr3 = new Node<Type>[tree1.getSize() + tree2.getSize()];

    tree1.tree2array(arr1);
    tree2.tree2array(arr2);

    mergeArrays(arr1, arr2, tree1.getSize(), tree2.getSize(), arr3);

    int i=0, count =0;
    while(i < (tree1.getSize() + tree2.getSize())){
        if(func(arr3[i])){
            count++;
        }
        i++;
    }

    if(0 == count){
        delete[]arr1;
        delete[]arr2;
        delete[]arr3;
        return;
    }

    Node<Type>* arr4 = new Node<Type>[count];

    i=0;
    int j=0;
    while(i < tree1.getSize() + tree2.getSize()){
        if(true == func(arr3[i])){
            arr4[j] = arr3[i];
            j++;
        }
        i++;
    }

    array2tree(merge_tree,arr4,count);

    delete[]arr1;
    delete[]arr2;
    delete[]arr3;
    delete[]arr4;

    merge_tree.size = count;
}





#endif //MIVNEI_AVL1_H
