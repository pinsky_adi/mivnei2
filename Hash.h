//
// Created by pinsk on 6/11/2018.
//

#ifndef MIVNEI2_HASH_H
#define MIVNEI2_HASH_H

#include "Clan.h"

class CLANALREADYEXIST{};

struct listNode{
    listNode* next;
    Clan* clan;

    explicit listNode() : next(NULL), clan(NULL) {};

};

struct list{

    listNode* head;

    explicit list() : head(NULL){};

    bool isEmpty(){
        return NULL == head;
    }





    Clan* search(int id){

        if(isEmpty()){
            return NULL;
        }

        listNode* x = head;
        while(NULL != x){
            if(x->clan->id == id){
                return x->clan;
            }
            x=x->next;
        }
        return NULL;

    }

    void insert(Clan* x){

        listNode* node = new listNode;
        node->clan = x;
        if(isEmpty()){
            head = node;
        }
        else{
            node->next = head;
            head = node;
        }
    }

    ~list(){
        listNode* x = head;
        while(NULL !=x){
            listNode* y = x->next;
            delete x;
            x=y;
        }
    }

};


struct Hash {

    //number of elements actually stored in hash table
    int n;

    //size of the hash table
    int m;

    list *arr;

    int hashFunction(int id) {
        return id % m;
    }

    explicit Hash(int m) : n(0), m(m) {
        arr = new list[m];
    }

    void tableDouble(){

        list* new_arr = new list[2*m];

        m = 2*m;

        for(int i=0;i<m/2;i++){
            if(!arr[i].isEmpty()){
                listNode* x = arr[i].head;

                while(NULL != x){
                    int key = hashFunction(x->clan->id);
                    new_arr[key].insert(x->clan);
                    x=x->next;
                }
            }
        }

        delete[] arr;
        arr= new_arr;
    }

    //if not found return NULL
    Clan *search(int id) {
        int key = hashFunction(id);
        return arr[key].search(id);
    }

    void insert(Clan* x){

        if(NULL != search(x->id)){
            throw CLANALREADYEXIST();
        }

        if(n == m-1){
            tableDouble();
        }

        int key = hashFunction(x->id);
        arr[key].insert(x);
        n++;
    }


    ~Hash() {
        delete[] arr;
    }
};

#endif //MIVNEI2_HASH_H
